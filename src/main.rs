// TODO: Change to use subcommands
//! Converts to or from Traditional Chinese characters.
#![warn(missing_docs)]

use anyhow::Result;
use clap::{ArgGroup, Parser};
use clipboard::{ClipboardContext, ClipboardProvider};
use std::path::Path;
use std::thread;
use std::time::Duration;

use opencc_rust::*;

/// CLI options
#[derive(Parser, Debug)]
#[clap(version)]
#[clap(about = "Convert between simplified and traditional characters.")]
#[clap(group = ArgGroup::new("convert").required(false).multiple(false))]
struct Opts {
    /// Convert to Traditional characters
    #[clap(short, long, group = "convert")]
    trad: bool,

    /// Convert to Simplified characters
    #[clap(short, long, group = "convert")]
    simp: bool,

    /// String to convert
    #[clap(takes_value = true)]
    string: Option<String>,

    /// File to convert
    #[clap(short, long)]
    file: Option<String>,

    /// Convert quotation marks in subtitles
    #[clap(short, long)]
    quotes: bool,
}

/// Convert to Traditional characters
fn to_trad(string: &str) -> String {
    let cc = OpenCC::new(DefaultConfig::S2TWP).unwrap();
    cc.convert(string)
}

/// Convert to Simplified characters
fn to_simp(string: &str) -> String {
    let cc = OpenCC::new(DefaultConfig::TW2SP).unwrap();
    cc.convert(string)
}

/// Copy converted text to clipboard
fn to_clipboard(clip: &mut ClipboardContext, string: &str) -> Result<()> {
    clip.set_contents(string.into())
        .expect("Can't set clipboard");
    thread::sleep(Duration::from_secs(10));
    Ok(())
}

/// Grab text from clipboard
fn from_clipboard(clip: &mut ClipboardContext) -> String {
    let string = clip.get_contents().expect("Can't set clipboard");
    string
}

/// Decide where input string comes from
fn get_input(opts: &Opts, clip: &mut ClipboardContext) -> Result<String> {
    let input = opts.string.clone().unwrap_or_else(|| from_clipboard(clip));

    Ok(input)
}

/// Read input file to convert
fn read_file<P>(path: P) -> Result<String>
where
    P: AsRef<Path>,
{
    let text = std::fs::read_to_string(path)?;
    Ok(text)
}

fn main() -> Result<()> {
    let opts = Opts::parse();
    let mut clip: ClipboardContext = ClipboardProvider::new().unwrap();

    let input = if let Some(ref file) = opts.file {
        read_file(file)?
    } else {
        get_input(&opts, &mut clip)?
    };

    let mut output = if opts.trad {
        to_trad(&input)
    } else if opts.simp {
        to_simp(&input)
    } else {
        input
    };

    if opts.quotes {
        output = output.replace('“', "「");
        output = output.replace('”', "」");
    }

    if opts.string.is_some() || opts.file.is_some() {
        println!("{}", output);
    } else {
        to_clipboard(&mut clip, &output)?;
    }

    Ok(())
}
